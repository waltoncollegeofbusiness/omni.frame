#!/bin/bash

# move to working directory
cd $( dirname "${BASH_SOURCE[0]}" )

for fn in `ls ../templates/`; do
    sed 's/<div id="php_include_div"><\/div>/<div id="php_include_div"><\?php include\("include\/index\.php"\) \?><\/div>/g' ../templates/$fn > ../${fn%%.html}.php
done
